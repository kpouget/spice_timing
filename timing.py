#! /usr/bin/python

import plotly.graph_objects as go
from plotly.subplots import make_subplots
import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Output, Input, State
import types

COLORS = [
    '#2ca02c',  # cooked asparagus green
    '#ff7f0e',  # safety orange
    '#1f77b4',  # muted blue
    '#d62728',  # brick red
    '#9467bd',  # muted purple
    '#8c564b',  # chestnut brown
    '#e377c2',  # raspberry yogurt pink
    '#7f7f7f',  # middle gray
    '#bcbd22',  # curry yellow-green
    '#17becf'   # blue-teal
]

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

input_ids = {
    'input-fps':30, 'input-pf-size':100, 'input-kf-size':500, 'input-net-bw':75, 'input-kfps': 11
}

PREDEFINED = {
    "Networks": {
        "ADSL": "net_bw=0.8", # &net_lat=30
        '4G': 'net_bw=1.7', # &net_lat=50
        'Optic Fiber': 'net_bw=10', #&net_lat=15
        'Etherner': 'net_bw=100', # &net_lat=10
    },
}

def get_predefined():
    html_lst = []
    for type_name, type_values in PREDEFINED.items():
        html_lst += [
            html.P(html.B(f"{type_name}:")),
            html.Ul([html.Li(html.A(name, href=f'#{link}'))
                     for name, link in type_values.items()])
        ]

    return html_lst

app.layout = html.Div(children=[
    html.Center(html.H3(children='SPICE frame streaming')),
    html.Div(className="row", children=[
        html.Div(className='four columns', children=[
            html.P(html.B("Encoding: ")),
            html.Ul([
                html.Li(["FPS: ", dcc.Input(id="input-fps", type="number", placeholder="FPS", step=1)]),
                html.Li(["Keyframes every ", dcc.Input(id="input-kfps", type="number", placeholder="keyframe period"), " frames"]),
            ]),
        ]),
        html.Div(className='four columns', children=[
            html.P(html.B("Frame sizes: ")),
            html.Ul([
                html.Li(["Keyframe size: ", dcc.Input(id="input-kf-size", type="number", placeholder="Key frame size",step=100), " KB"]),
                html.Li(["P frame size: ", dcc.Input(id="input-pf-size", type="number", placeholder="P frame size", step=10), " KB"]),
            ]),
        ]),
        html.Div(className='four columns', children=[
            html.P(html.B("Network: ")),
            html.Ul([
                html.Li(["Bandwidth: ", dcc.Input(id="input-net-bw", type="number", placeholder="network bandwidth"), " MB/s"])
            ]),
        ])
    ]),
    dcc.Graph(id='donut-graph'),
    html.P(id='txt'),
    dcc.Location(id='url', refresh=False), html.P(html.A('Permalink', id='permalink')),
    html.Hr(),

] + get_predefined())

@app.callback([Output(input_id, 'value') for input_id in input_ids],
              [Input('url', 'hash')],
              [State(input_id, 'value') for input_id in input_ids])
def load_permalink(hash_val, *state):
    if None in state: current_values = input_ids # default values
    else: current_values = dict(zip(input_ids, state))

    if hash_val:
        new_keys = ["input-"+kv.split('=')[0].replace("_", "-") for kv in hash_val[1:].split("&")]
        new_values = [float(kv.split('=')[1]) for kv in hash_val[1:].split("&")]

        current_values.update(dict(zip(new_keys, new_values)))

    return [current_values[k] for k in input_ids] # ensure order

def s_to_ms(t): return int(t*1000)

@app.callback([Output('donut-graph', 'figure'), Output('txt', 'children')]+[Output('permalink', 'href')],
              [Input(input_id, 'value') for input_id in input_ids])
def draw(*input_values):
    if None in input_values: return dash.no_update, dash.no_update, dash.no_update
    if 0 in input_values: return dash.no_update, dash.no_update, dash.no_update

    inputs = types.SimpleNamespace()


    out = types.SimpleNamespace()
    out.kf = types.SimpleNamespace()
    out.pf = types.SimpleNamespace()
    inputs.__dict__.update(dict(zip([id.partition("-")[-1].replace('-', '_') for id in input_ids], input_values)))

    out.fps_period = s_to_ms(1/inputs.fps)
    out.kf.transfer_bw = s_to_ms(inputs.kf_size/1000 / inputs.net_bw)
    out.kf.transfer = out.kf.transfer_bw
    out.kf.process = out.fps_period - out.kf.transfer

    out.pf.transfer_bw = s_to_ms(inputs.pf_size/1000 / inputs.net_bw)
    out.pf.transfer = out.pf.transfer_bw
    out.pf.process = out.fps_period - out.pf.transfer

    out.kf.nb = inputs.fps / inputs.kfps
    out.pf.nb = inputs.fps - out.kf.nb
    out.kf.bw = out.kf.nb * inputs.kf_size
    out.pf.bw = out.pf.nb * inputs.pf_size
    out.bw = out.kf.bw + out.pf.bw

    out.free_bw = inputs.net_bw*1000 - out.bw

    # Create subplots: use 'domain' type for Pie subplot
    fig = make_subplots(rows=1, cols=3, specs=[[{'type':'domain'}, {'type':'domain'}, {'type':'domain'}]])

    for i, (name, frame) in enumerate((["Key frame processing (in ms)", out.kf], ["P frame processing (in ms)", out.pf])):
        infos = {
            "Processing": frame.process,
            "Transfer": frame.transfer_bw,
        }

        fig.add_trace(go.Pie(labels=list(infos.keys()), marker=dict(colors=COLORS[:3]),
                             values=list(infos.values()), hoverlabel= {'namelength' :-1},
                             name=name, title=dict(text=name, position='bottom center'),
                             pull=[0.2, 0]),
                      1, 1+i)

    fig.add_trace(go.Pie(labels=["Available", "Key frames", "P frames"], name="Network",
                         values=list(map(int, [out.free_bw/1000, out.kf.bw/1000, out.pf.bw/1000])),
                         marker=dict(colors=COLORS[:3]), hoverlabel= {'namelength' :-1},
                         title=dict(text="Network bandwidth (in MB/s)", position='bottom center'),
                         pull=[0.2, 0]),
                         1, 3)
    fig.update_traces(hoverinfo="label+name", textinfo='label+value')

    fig.update_layout(showlegend=False)

    msgs = [
        f"A new frame is displayed every {out.fps_period}ms.",

        [html.U(f"In 1 second"), ", there are ", html.B(f"{out.kf.nb:.1f} keyframes"), " and ", html.B(f"{out.pf.nb:.1f} P frames"),
        ", so a ", html.B(f"bandwidth of {out.kf.bw:.1f} + {out.pf.bw:.1f} = {out.bw:.1f} KB/s ({out.bw/1000:.1f} MB/s)"), (f". That's {-out.free_bw/1000} MB/s TOO MUCH!" if out.free_bw < 0 else "")],
        [html.B("Key frames: "), "" if out.kf.process >= 0 else f" CANNOT WORK, {-out.kf.process}ms too short.",
         html.Ul([
             html.Li(f"They take {out.kf.transfer}ms to be transferred"),
             html.Li(f"That leaves {out.kf.process}ms for their processing (deadline: {out.fps_period}ms - total transfer time: {out.kf.transfer}ms)"),
             ]
         )],
        [html.B("P frames:"), "" if out.pf.process >= 0 else f" CANNOT WORK, {-out.kf.process}ms too short.",
         html.Ul([
             html.Li(f"They takes {out.pf.transfer}ms to be transferred"),
             html.Li(f"That leaves {out.pf.process}ms for their processing (deadline: {out.fps_period}ms - total transfer time: {out.pf.transfer}ms)"),
             ]
         )],
    ]

    hash_val = '#'+"&".join(f"{k}={v}" for k, v in inputs.__dict__.items())
    return fig, html.Ul([html.Li(msg) for msg in msgs]), hash_val


if __name__ == '__main__':
    app.run_server(debug=True, port=8051)
else:
    application = app.server
