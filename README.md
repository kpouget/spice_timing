This is a small tool to compute 'theoretical' deadlines when transfering video frames (big keyframes and small P frames) over the network.

INSTALL
-------

```
pipenv install
pipenv run ./timing.py
xdg-open http://127.0.0.1:8051/
```

Example:
--------

http://127.0.0.1:8051/#fps=60&pf_size=100&kf_size=500&net_bw=75&kfps=10

![example](screenshot.png)
